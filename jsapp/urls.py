from django.conf.urls import patterns, include, url
from django.contrib import admin
from jsapp import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Presentacion.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

	url(r'^llamadajsapp/$', views.llamadajsapp),
	url(r'^llamadadialogo/$', views.llamadadialogo),
	url(r'^llamadaanimacion/$', views.llamadaanimacion),
	url(r'^llamadajQuery/$', views.llamadajQuery),
	url(r'^llamadajQuery2/$', views.llamadajQuery2),
	url(r'^llamadajQuery3/$', views.llamadajQuery3),
)
