from django.shortcuts import render

def llamadajsapp(request):
    return render(request, 'jsapp/jsapp.html', {})
def llamadadialogo(request):
	return render(request, 'jsapp/dialogo.html',{})
def llamadaanimacion(request):
	return render(request, 'jsapp/animacion.html',{})
def llamadajQuery(request):
	return render(request, 'jsapp/jQuery.html',{})
def llamadajQuery2(request):
	return render(request, 'jsapp/jQuery2.html',{})
def llamadajQuery3(request):
	return render(request, 'jsapp/jQuery3.html',{})
