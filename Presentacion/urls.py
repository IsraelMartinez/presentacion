from django.conf.urls import patterns, include, url
from django.contrib import admin
from Presentacion import views


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Presentacion.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'Presentacion.views.home', name='home'),
    
    url(r'^Presentacion/', include('Presentacion.urls')),
    url(r'^djangoapp/', include('djangoapp.urls')),
    url(r'^jsapp/', include('jsapp.urls')),
    url(r'^webserviceapp/', include('webserviceapp.urls')),

    url(r'^llamadaNosotros/$', views.llamadaNosotros),
    url(r'^llamadaDescarga/$', views.llamadaDescarga),

)