from django.shortcuts import render

def llamadaDjangoapp(request):
    return render(request, 'djangoapp/djangoapp.html', {})

def llamadahtml5(request):
    return render(request, 'djangoapp/html5.html', {})

def llamadacss(request):
    return render(request, 'djangoapp/css.html', {})

def llamadaDjangoapp2(request):
    return render(request, 'djangoapp/djangoapp2.html', {})

def llamadaVacio(request):
    return render(request, 'djangoapp/vacio.html', {})