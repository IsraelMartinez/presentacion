from django.conf.urls import patterns, include, url
from django.contrib import admin
from djangoapp import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Presentacion.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

	url(r'^llamadaDjangoapp/$', views.llamadaDjangoapp),
	url(r'^llamadaDjangoapp2/$', views.llamadaDjangoapp2),
	url(r'^llamadahtml5/$', views.llamadahtml5),
	url(r'^llamadacss/$', views.llamadacss),
	url(r'^llamadaVacio/$', views.llamadaVacio),

)
