from django.conf.urls import patterns, include, url
from django.contrib import admin
from webserviceapp import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Presentacion.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

	
	url(r'^llamadawebserviceapp/$', views.llamadawebserviceapp),
	url(r'^llamadaImplementacion/$', views.llamadaImplementacion),
	url(r'^llamadaObservatorio/$', views.llamadaObservatorio),

)
