from django.shortcuts import render
import json
import urllib2
def llamadawebserviceapp(request):
	return render(request, 'webserviceapp/webserviceapp.html', {})

def llamadaImplementacion(request):
	context= {"tweets":(urllib2.urlopen("http://192.168.43.82:8080/WebServiceTweets/webresources/entities.tweet/0/20").read())}
	return render(request, 'webserviceapp/Implementacion.html', context)

def llamadaObservatorio(request):
	return render(request, 'webserviceapp/observatorio.html', {})
